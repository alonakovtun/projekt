﻿using Projekt.View;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Projekt
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class JanPage : ContentPage
	{
		public JanPage()
		{
			SetValue(NavigationPage.HasNavigationBarProperty, false);
			InitializeComponent();

			photo.Source = ImageSource.FromUri(new Uri("https://paga.org.pl/wp-content/uploads/2020/02/jan-kowalski-e1581058486869-240x240.jpg"));
		}

		async void Button_Clicked(object sender, EventArgs e)
		{
			await Navigation.PushAsync(new LoginPage());
		}
	}
}