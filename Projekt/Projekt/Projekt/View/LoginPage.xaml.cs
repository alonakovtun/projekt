﻿using Projekt.Tables;
using SQLite;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Projekt.View
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class LoginPage : ContentPage
	{
		public LoginPage()
		{
			SetValue(NavigationPage.HasNavigationBarProperty, false);
			InitializeComponent();
		}

		async void Button_Clicked(object sender, EventArgs e)
		{
			
			await Navigation.PushAsync(new RegistrationPage());
		}

		async void Button_Clicked_1(object sender, EventArgs e)
		{
			var path = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments), "UserDatabase.db");
			var db = new SQLiteConnection(path);
			var myquery = db.Table<RegUserTable>().Where(u => u.UserName.Equals(EntryUser.Text) && u.Password.Equals(EntryPassword.Text)).FirstOrDefault();



			if (myquery.UserName.Equals("Alona") && myquery.Password.Equals("123"))
			{
				App.Current.MainPage = new NavigationPage(new HomePage());
			}
			else if(myquery.UserName.Equals("Jan") && myquery.Password.Equals("123"))
			{
				App.Current.MainPage = new NavigationPage(new JanPage());
			}
			else
			{
				Device.BeginInvokeOnMainThread(async () =>
				{
					var result = await this.DisplayAlert("Error", "Failed UserName or Password", "Yes", "Cancel");
					if (result)
						await Navigation.PushAsync(new LoginPage());
					else
					{
						await Navigation.PushAsync(new LoginPage());
					}
				});
			}


		}
	}
}