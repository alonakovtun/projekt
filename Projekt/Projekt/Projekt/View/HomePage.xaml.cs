﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using System.Reflection;
using System.IO;

namespace Projekt.View
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class HomePage : ContentPage
	{
		public HomePage()
		{
			SetValue(NavigationPage.HasNavigationBarProperty, false);
			InitializeComponent();

			photo.Source = ImageSource.FromUri(new Uri("https://scontent-waw1-1.xx.fbcdn.net/v/t1.0-9/s851x315/64591745_606947599815388_9020803804169240576_o.jpg?_nc_cat=110&_nc_sid=da31f3&_nc_ohc=5PWY-ltZzmwAX8AAAYM&_nc_ht=scontent-waw1-1.xx&_nc_tp=7&oh=cca82c12887467354a0cbc215e1b0c32&oe=5EFA899A"));
		}

		async void Button_Clicked(object sender, EventArgs e)
		{
			await Navigation.PushAsync(new LoginPage());
		}



		
	}
}