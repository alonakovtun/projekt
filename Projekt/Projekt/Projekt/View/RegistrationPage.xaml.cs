﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using System.IO;
using SQLite;
using Projekt.Tables;

namespace Projekt.View
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class RegistrationPage : ContentPage
	{
		public RegistrationPage()
		{
			SetValue(NavigationPage.HasNavigationBarProperty, false);
			InitializeComponent();
		}

		private void Button_Clicked(object sender, EventArgs e)
		{
			var path = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments), "UserDatabase.db");
			var db = new SQLiteConnection(path);
			db.CreateTable<RegUserTable>();

			var item = new RegUserTable()
			{
				UserName = EntryUserName.Text,
				Password = EntryUserPassword.Text,
				Email = EntryUserEmail.Text,
				PhoneNumber = EntryUserPhoneNumber.Text

			};

			db.Insert(item);
			Device.BeginInvokeOnMainThread(async () =>
			{	
				var result = await this.DisplayAlert("Congratulation", "User Registration Sucessfull", "Yes", "Cancel");
				if (result)
					await Navigation.PushAsync(new LoginPage());
			});


		}

		async void Button_Clicked_1(object sender, EventArgs e)
		{
			await Navigation.PushAsync(new LoginPage());
		}
	}
}